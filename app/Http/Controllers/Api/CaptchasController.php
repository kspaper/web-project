<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Gregwar\Captcha\CaptchaBuilder;
use App\Http\Requests\Api\CaptchaRequest;
//use App\Http\Controllers\Controller;

class CaptchasController extends Controller
{
    public function store(CaptchaRequest $request, CaptchaBuilder $captchaBuilder)
    {
        //CaptchaRequest 要求用户必须通过手机号调用 图片验证 接口
        //CaptchaBuilder 通过其 build() 方法，创建出来验证码图片
        $key = 'captcha-'.str_random(15);
        $phone = $request->phone;

        $captcha = $captchaBuilder->build();
        $expiredAt = now()->addMinutes(2);
        \Cache::put($key, ['phone' => $phone, 'code' => $captcha->getPhrase()], $expiredAt);
        //getPhrase() 方法获取验证码文本

        $result = [
            'captcha_key' => $key,
            'expired_at' => $expiredAt->toDateTimeString(),
            'captcha_image_content' => $captcha->inline()
            //inline() 方法获取base64图片验证码
            //因为图片小，所以可以用base64格式返回
        ];

        return $this->response->array($result)->setStatusCode(201);
    }
}