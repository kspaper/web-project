<?php

namespace Tests\Feature;

use App\Models\User;
use App\Models\Topic;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\Traits\ActingJWTUser;

//200 OK - GET PUT PATCH or DELETE
//201 Created
//204 No Content
//404 Not Found - Request a null resource
class TopicApiTest extends TestCase
{
    use ActingJWTUser;

    protected $user;
    //全局变量，整个类可以用

    public function setUp()
    {
        //setUp 方法会在测试开始之前执行

        parent::setUp();

        //我们先创建一个用户，测试会以该用户的身份进行测试

        $this->user = factory(User::class)->create();

        //并赋值给全局变量
    }

    public function testStoreTopic()
    {
        $data = ['category_id' => 1, 'body' => 'test body', 'title' => 'test title'];
        /*
        * 因为此处会重复用到，所以要进行封装
        $token = \Auth::guard('api')->fromUser($this->user);
        $response = $this->withHeaders(['Authorization' => 'Bearer '.$token])
            ->json('POST', '/api/topics', $data);
        */

        //$this->json 可以方便的模拟各种 HTTP 请求
        //第一个参数: 请求的方法，发布话题使用的是 POST 方法
        //第二个参数: 请求地址，请求 /api/topics
        //第三个参数: (内容) - 请求参数，传入 category_id，body，title，这三个必填参数
        //第四个参数: 请求 Header, 可以直接设置 header，也可以利用 withHeaders 方法达到同样的目的

        $response = $this->JWTActingAs($this->user)
            ->json('POST', '/api/topics', $data);
        //通过 use Tests\Traits\ActingJWTUser; 找到文件
        //通过 use ActingJWTUser; 找到封装的父类
        //通过使用 JWTActingAs 方法，登录一个用户
        //最后，得到的响应 $response

        $assertData = [
            'category_id' => 1,
            'user_id' => $this->user->id,
            'title' => 'test title',
            'body' => clean('test body', 'user_topic_body'),
        ];

        $response->assertStatus(201)
            ->assertJsonFragment($assertData);
        //通过 assertStatus 断言响应结果为 201
        //通过 assertJsonFragment 断言响应结果包含 assertData 数据
    }

    public function testUpdateTopic()
    {
        $topic = $this->makeTopic();

        $editData = ['category_id' => 2, 'body' => 'edit body', 'title' => 'edit title'];

        $response = $this->JWTActingAs($this->user)
            ->json('PATCH', '/api/topics/'.$topic->id, $editData);

        $assertData= [
            'category_id' => 2,
            'user_id' => $this->user->id,
            'title' => 'edit title',
            'body' => clean('edit body', 'user_topic_body'),
        ];

        $response->assertStatus(200)
            ->assertJsonFragment($assertData);
    }
    //要修改话题，首先需要为用户创建一个话题
    protected function makeTopic()
    {
        return factory(Topic::class)->create([
            'user_id' => $this->user->id,
            'category_id' => 1,
        ]);
    }

    public function testShowTopic()
    {
        $topic = $this->makeTopic();
        //先创建一个话题
        $response = $this->json('GET', '/api/topics/'.$topic->id);
        //然后访问 话题详情 接口
        $assertData= [
            'category_id' => $topic->category_id,
            'user_id' => $topic->user_id,
            'title' => $topic->title,
            'body' => $topic->body,
        ];
        //断言响应状态码为 200 以及响应数据与刚才创建的话题数据一致
        $response->assertStatus(200)
            ->assertJsonFragment($assertData);
    }

    public function testIndexTopic()
    {
        $response = $this->json('GET', '/api/topics');

        $response->assertStatus(200)
            ->assertJsonStructure(['data', 'meta']);
    }

    public function testDeleteTopic()
    {
        $topic = $this->makeTopic();
        $response = $this->JWTActingAs($this->user)
            ->json('DELETE', '/api/topics/'.$topic->id);
        $response->assertStatus(204);

        $response = $this->json('GET', '/api/topics/'.$topic->id);
        $response->assertStatus(404);
    }
}